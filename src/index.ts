import "reflect-metadata";
import { createConnection } from "typeorm";
import { Todo } from "./entity/Todo";
import * as express from "express";
import * as cors from 'cors';
import { getAll, save, edit } from "./todoController";

createConnection().then((connection) => {
  // create and setup express app
  const app = express();
  const corsApp = cors();
  app.use(express.json());
  app.use(corsApp)

  // register routes

  app.get("/todos", async (req, res) => {
    const todos = await getAll();
    return res.send(todos);
  });

  app.post("/todo", async (req, res) => {
    const todo = await save(req.body);
    return res.send(todo);
  });

  app.put("/todo", async (req, res) => {
    const todo = await edit(Number(req.query.id), req.body);
    return res.send(todo);
  });
  app.listen(3010);
  
});

// createConnection()
//   .then(async (connection) => {
//     console.log("Inserting a new user into the database...");
//     const todo = new Todo();
//     todo.text = "test JS";
//     todo.done = false;
//     let todoRepository = connection.getRepository(Todo);
//     await todoRepository.save(todo);
//     console.log("Saved a new user with id: " + todo.todo_id);

//     console.log("Loading users from the database...");
//     const users = await todoRepository.find();
//     console.log("Loaded users: ", users);

//     console.log("Here you can setup and run express/koa/any other framework.");
//   })
//   .catch((error) => console.log(error));
