import { Todo } from "./entity/Todo";
import { getRepository, getConnection } from "typeorm";
import { Request, Response } from "express";

export function getAll(): Promise<Todo[]> {
  return getRepository(Todo).find();
}

export function save(todo: Todo): Promise<Todo> {
  return getRepository(Todo).save(todo);
}

export function edit(id: number, todo: Todo): Promise<Todo> {
  getConnection()
    .createQueryBuilder()
    .update(Todo)
    .set({
      edited: () => 'datetime("now","localtime")',
      text: todo.text,
      done: todo.done,
    })
    .where("todo_id = :id", { id: id })
    .execute();

  return getRepository(Todo).findOne(id);
}
