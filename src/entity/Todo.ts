import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class Todo {
  @PrimaryGeneratedColumn({name: 'todo_id'})
  id: number;

  @CreateDateColumn({ type: "date", default: () => 'datetime("now","localtime")'})
  created: Date;

  @Column({ type: "date", default: () => 'datetime("now","localtime")'})
  edited: Date;

  @Column({ type: "text" })
  text: string;

  @Column({ type: "int", default: false })
  done: boolean;
}
